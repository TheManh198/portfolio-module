<?php

namespace HTCMage\Portfolio\Model\ResourceModel\PortfolioManagement;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';
    
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \HTCMage\Portfolio\Model\PortfolioManagement::class,
            \HTCMage\Portfolio\Model\ResourceModel\PortfolioManagement::class
        );
    }
}
