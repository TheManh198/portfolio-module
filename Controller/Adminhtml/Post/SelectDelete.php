<?php

namespace HTCMage\Portfolio\Controller\Adminhtml\Post;

use Exception;
use HTCMage\Portfolio\Model\PortfolioManagementFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use HTCMage\Portfolio\Model\ResourceModel\PortfolioManagement;

class SelectDelete extends Action
{

    /**
     * @var CollectionFactory
     */
    protected $portfolioManagementFactory;

    /**
     * @var ResourceModel\PortfolioManagement|PortfolioManagement
     */
    private $resource;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $CollectionFactory
     */
    public function __construct(
        Action\Context $context, 
        PortfolioManagementFactory $portfolioManagementFactory, 
        PortfolioManagement $resource
    ){
        $this->portfolioManagementFactory = $portfolioManagementFactory;
        $this->resource = $resource;
        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return Redirect
     * @throws LocalizedException|Exception
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {

            try {
                $model = $this->getById($id);
                
                $this->delete($model);
                $this->messageManager->addSuccessMessage(
                    __('Att has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('htcmage_portfolio/post/index');
            }
        }

        $this->messageManager->addError(__('We can\'t find a portfolio to delete.'));
        return $resultRedirect->setPath('htcmage_portfolio/post/index');
    }

    public function getById($id = null)
    {
        $model = $this->portfolioManagementFactory->create();
        if ($id) {
            $this->resource->load($model, $id);
        }
        return $model;
    }

    public function delete($modelPortfolio)
    {
        try {
            $this->resource->delete($modelPortfolio);
        } catch (Exception $exception) {
            return false;
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('HTCMage_Portfolio::selectDelete');
    }
}
