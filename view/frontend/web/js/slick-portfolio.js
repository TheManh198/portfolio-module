define([
    'jquery',
    'mage/template',
    'HTCMage_Portfolio/js/slick.min',
    'domReady!'
], function($, mageTemplate){
    'use strict';
    $.widget('htcmage.portfolio', {
        
        _create: function() {
            let $widget = this;
            // var element = $widget.element;
            // console.log($widget);
            
            $('.portfolio-box-slider').slick({
                slidesToShow: $widget.options.limit,
		        slidesToScroll: 1,
		        autoplay: true,
		        autoplaySpeed: 2000
            });

        }
    });

    return $.htcmage.portfolio;
});
