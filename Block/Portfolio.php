<?php

namespace HTCMage\Portfolio\Block;

use HTCMage\Portfolio\Model\PortfolioManagementFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Portfolio extends Template implements BlockInterface
{
    protected $_template = "widget/view.phtml";

    protected $portfolioManagementFactory;

	public function __construct(
		Context $context,
		PortfolioManagementFactory $portfolioManagementFactory,
		StoreManagerInterface $storeManager,
		\Magento\Framework\View\Page\Config $pageConfig,		
		array $data = []
	){
		$this->portfolioManagementFactory = $portfolioManagementFactory;
		$this->storeManager = $storeManager;
    	$this->_pageConfig = $pageConfig;   
		parent::__construct($context, $data);
	}

	public function getPortfolioCollection()
	{
		$limit = $this->getData('limit');
		$portfolio = $this->portfolioManagementFactory->create();
		$portfolio->getCollection()->setOrder('id','ASC');
		return $portfolio->getCollection()->setPageSize($limit);
	}

	public function getPortfolioImageUrl($item)
	{
		return $this->getMediaUrl() . $item->getImage();
	}

	/**
     * @return string
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'htcmage/portfolio/';
        return $mediaUrl;
    }
}