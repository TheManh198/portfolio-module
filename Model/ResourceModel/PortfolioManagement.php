<?php

namespace HTCMage\Portfolio\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class PortfolioManagement extends AbstractDb
{
    
    const MAIN_TABLE ='htcmage_portfolio_management';

    protected function _construct()
    {
        $this->_init(static::MAIN_TABLE, 'id');
    }

}
