<?php

namespace HTCMage\Portfolio\Model;

use Magento\Framework\Model\AbstractModel;

class PortfolioManagement extends AbstractModel 
{
    protected $_idFieldName = 'id';
    
    protected function _construct()
    {
        $this->_init(\HTCMage\Portfolio\Model\ResourceModel\PortfolioManagement::class);
    }
}