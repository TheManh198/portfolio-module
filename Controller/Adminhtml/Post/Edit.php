<?php

namespace HTCMage\Portfolio\Controller\Adminhtml\Post;

use HTCMage\Portfolio\Model\PortfolioManagement;
use HTCMage\Portfolio\Model\PortfolioManagementFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Maxime\Jobs\Model\Department;

class Edit extends Action
{
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $registry = null;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var Department
     */
    protected $_model;

    /**
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param Department $model
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        PortfolioManagement $model
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->_model = $model;
        parent::__construct($context);
    }

    /**
     * @return Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_model;
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This portfolio not exists.'));
                $resultRedirect = $this->_resultRedirectFactory->create();

                return $resultRedirect->setPath('*/htcmage_portfolio/');
            }
        }
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);

        }
        $this->registry->register('portfolio', $model);
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Portfolio') : __('New Portfolio'),
            $id ? __('Edit Portfolio') : __('New Portfolio')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Portfolio Management'));
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? $model->getName() : __('New Portfolio'));
        return $resultPage;
    }

    /**
     * Init actions
     *
     * @return Page
     */
    protected function _initAction()
    {
        /** @var Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('HTCMage_Portfolio::portfolio')
            ->addBreadcrumb(__('Portfolio Management'), __('Portfolio Management'))
            ->addBreadcrumb(__('Manage Portfolio'), __('Manage Portfolio'));
        return $resultPage;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('HTCMage_Portfolio::save');
    }
}