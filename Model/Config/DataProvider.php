<?php
 
namespace HTCMage\Portfolio\Model\Config;
 
use HTCMage\Portfolio\Model\PortfolioManagementFactory;
use HTCMage\Portfolio\Model\ResourceModel\PortfolioManagement\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;
 
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $_loadedData;
    protected $collection;
    private $dataPersistor;
 
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        DataPersistorInterface $dataPersistor,
        CollectionFactory $collectionFactory,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
 
    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $item) {
            $this->_loadedData[$item->getId()] = $item->getData();
            $fullData = $this->_loadedData;
            if ($item->getImage()) {
                $m['image'][0]['name'] = $item->getImage();
                $m['image'][0]['url'] = $this->getMediaUrl() . $item->getImage();
            }
            $this->_loadedData[$item->getId()] = array_merge($fullData[$item->getId()], $m);
        }
        $data = $this->dataPersistor->get('portfolio');

        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->_loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear('portfolio');
        }
        return $this->_loadedData;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        $mediaUrl = $this->storeManager->getStore()
                ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'htcmage/portfolio/';
        return $mediaUrl;
    }
}