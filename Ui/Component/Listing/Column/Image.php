<?php

namespace HTCMage\Portfolio\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class Image extends Column
{
    /**
     *
     */
    const NAME = 'thumbnail';
    /**
     *
     */
    const ALT_FIELD = 'name';
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Repository
     */
    private $assetRepo;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storeManager,
        Repository $assetRepo,
        array $components = [],
        array $data = []
    )
    {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
        $this->assetRepo = $assetRepo;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $path = $this->storeManager->getStore()->getBaseUrl(
                    UrlInterface::URL_TYPE_MEDIA
                ) . 'htcmage/portfolio/';

            $baseImage = $this->assetRepo->getUrl('HTCMage_Portfolio::images/icon.jpg');
            foreach ($dataSource['data']['items'] as & $item) {
                if ($item['image']) {
                    $item['image' . '_src'] = $path . $item['image'];
                    $item['image' . '_alt'] = $item['name'];
                    $item['image' . '_orig_src'] = $path . $item['image'];
                } else {
                    $item['image' . '_src'] = $baseImage;
                    $item['image' . '_alt'] = 'Att';
                    $item['image' . '_orig_src'] = $baseImage;
                }
            }
        }

        return $dataSource;
    }
}
