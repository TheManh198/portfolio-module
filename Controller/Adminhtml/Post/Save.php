<?php
 
namespace HTCMage\Portfolio\Controller\Adminhtml\Post;
 
use Magento\Backend\App\Action;
use HTCMage\Portfolio\Model\PortfolioManagementFactory;
use HTCMage\Portfolio\Model\Config\ImageUploader;
use Magento\Backend\Model\View\Result\RedirectFactory;
 
class Save extends Action
{
    private $resultRedirect;
    private $portfolioManagementFactory;
    protected $imageUploader;
 
    public function __construct(
        Action\Context $context,
        PortfolioManagementFactory $portfolioManagementFactory,
        RedirectFactory $redirectFactory,
        ImageUploader $imageUploader
    )
    {
        parent::__construct($context);
        $this->portfolioManagementFactory = $portfolioManagementFactory;
        $this->resultRedirect = $redirectFactory;
        $this->imageUploader = $imageUploader;
    }
 
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $id = !empty($data['id']) ? $data['id'] : null;
        if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $this->imageUploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = '';
        }
       
        $newData = [
            'name' => $data['name'],
            'status' => $data['status'],
            'description' => $data['description'],
            'image' => $data['image'],
        ];
 
        $post = $this->portfolioManagementFactory->create();
        if ($id) {
            $post->load($id);
            $this->getMessageManager()->addSuccessMessage(__('Edit Success'));
        } else {
            $this->getMessageManager()->addSuccessMessage(__('Edit Failed'));
        }
        try{
            $post->addData($newData);
            $post->save();
            $this->getMessageManager()->addSuccessMessage(__('Save Success'));
            return $this->resultRedirect->create()->setPath('htcmage_portfolio/post/index');
        }catch (\Exception $e){
            $this->getMessageManager()->addErrorMessage(__('Save Failed'));
        }

    }

    /**
     * @return mixed
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('HTCMage_Portfolio::save');
    }
}