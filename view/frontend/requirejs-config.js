var config = {
    paths: {
        slick: 'HTCMage_Portfolio/js/slick.min'
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }
};